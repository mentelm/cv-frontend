import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  public languages = ['pl', 'en'];


  constructor(private translateService: TranslateService) {}

  public change(lang: string) {
    this.translateService.use(lang);
  }
}
