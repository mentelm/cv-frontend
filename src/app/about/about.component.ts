import {Component, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit {
  text = [];

  private url = environment.backendUrl + '/paragraphs';


  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get<string[]>(this.url).subscribe(
      response => {
        this.text = response;
      }
    );
  }

}
