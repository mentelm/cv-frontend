export class Info {
  key: string;
  value: string;
  link?: string;
}
