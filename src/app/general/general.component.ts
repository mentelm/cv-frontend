import {Component, OnInit} from '@angular/core';
import {Info} from './info.model';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html'
})
export class GeneralComponent implements OnInit {
  infos: Info[] = [];

  private url = environment.backendUrl + '/general';


  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get<Info[]>(this.url).subscribe(
      response => {
        this.infos = response;
      }
    );
  }

}
