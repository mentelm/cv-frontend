import {EventEmitter, Injectable, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Skill, SkillCategory} from './skill.model';
import {formatNumber} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {
  private url = environment.backendUrl + '/skills';
  private skills: EventEmitter<SkillCategory[]> = new EventEmitter<SkillCategory[]>();

  constructor(private http: HttpClient) {
    this.http.get<SkillApi[]>(this.url).subscribe(
      response => {
        this.skills.emit(SkillsApiMapper.toCategories(response));
      }
    );
  }

  public getSkills(): Observable<SkillCategory[]> {
    return this.skills;
  }
}

class SkillsApiMapper {
  public static toCategories(sourceApis: SkillApi[]): SkillCategory[] {
    let skillCategories = new Map<string, SkillCategory>();

    for (let api of sourceApis) {
      let skillCategory;

      if (!skillCategories.has(api.categoryName)) {
        skillCategory = new SkillCategory();
        skillCategory.name = api.categoryName;
        skillCategories.set(api.categoryName, skillCategory);
      } else {
        skillCategory = skillCategories.get(api.categoryName);
      }

      let skill = new Skill();
      skill.info = api.info;
      skill.level = api.level;
      skill.name = api.name;

      skill.color = SkillsApiMapper.getColor(api.level);

      skillCategory.skills.push(skill);
    }

    let categories: SkillCategory[] = [];
    for (let skill of skillCategories.values()) {
      categories.push(skill);
    }

    for (let category of categories) {
      category.skills.sort((a, b) => a.level == b.level ? 0 : (a.level > b.level ? -1 : 1));
    }

    return categories;
  }

  private static getColor(percentage: number): string {
    if (percentage < 30) {
      return 'danger';
    } else if (percentage < 50) {
      return 'warning';
    } else if (percentage < 70) {
      return 'success';
    } else {
      return 'info';
    }
  }
}

export class SkillApi {
  public name: string;
  public level: number;

  public info?: string;
  public categoryName: string;
}
