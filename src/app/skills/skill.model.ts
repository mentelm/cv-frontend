export class Skill {
  public name: string;
  public level: number;
  public color: string;

  public info?: string;
}

export class SkillCategory {
  public name: string;
  public skills: Skill[] = [];
}
