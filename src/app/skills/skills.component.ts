import {Component, OnInit} from '@angular/core';
import {SkillCategory} from './skill.model';
import {SkillsService} from './skills.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html'
})
export class SkillsComponent implements OnInit {
  skills: SkillCategory[] = [];

  constructor(private service: SkillsService) {
  }

  ngOnInit(): void {
    this.service.getSkills().subscribe(nxt => this.skills = nxt);
  }

}
