import {Component, OnInit} from '@angular/core';
import {Timeline} from './timelines.model';
import {TimelinesService} from './timelines.service';

@Component({
  selector: 'app-timelines',
  templateUrl: './timelines.component.html'
})
export class TimelinesComponent implements OnInit {
  constructor(private service: TimelinesService) {
  }

  timelines: Timeline[] = [];

  ngOnInit(): void {
    this.service.getTimelines().subscribe(nxt => this.timelines = nxt);
  }
}
