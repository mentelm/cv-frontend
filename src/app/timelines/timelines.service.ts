import {EventEmitter, Injectable, OnInit} from '@angular/core';
import {Timeline, TimelineEvent} from './timelines.model';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimelinesService {
  private url = environment.backendUrl + '/events';
  private timelines: EventEmitter<Timeline[]> = new EventEmitter<Timeline[]>();

  constructor(private http: HttpClient) {
    this.http.get<TimelineEventAPI[]>(this.url).subscribe(
      response => {
        this.timelines.emit(TimelineEventAPIMapper.toTimelines(response));
      }
    );
  }

  public getTimelines(): Observable<Timeline[]> {
    return this.timelines;
  }
}

class TimelineEventAPIMapper {
  public static toTimelines(sourceApis: TimelineEventAPI[]): Timeline[] {
    let timelines = new Map<string, Timeline>();

    for (let api of sourceApis) {
      let timeline: Timeline;

      if (!timelines.has(api.timelineName)) {
        timeline = new Timeline();
        timeline.name = api.timelineName;
        timelines.set(api.timelineName, timeline);
      } else {
        timeline = timelines.get(api.timelineName);
      }

      timeline.events.push(api);
    }

    let result: Timeline[] = [];
    for (let timeline of timelines.values()) {
      result.push(timeline);
    }
    return result;
  }
}

class TimelineEventAPI extends TimelineEvent {
  public timelineName: string;
}

