export class Timeline {
  public name: string;
  public events: TimelineEvent[] = [];
}

export class TimelineEvent {
  public from: string;
  public to?: string;
  public name: string;
  public company: string;
  public details?: string;
}
