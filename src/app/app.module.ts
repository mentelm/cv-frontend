import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {SkillsComponent} from './skills/skills.component';
import {TimelinesComponent} from './timelines/timelines.component';
import {AboutComponent} from './about/about.component';
import {GeneralComponent} from './general/general.component';
import {HeaderComponent} from './header/header.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {SkillsService} from './skills/skills.service';
import {TimelinesService} from './timelines/timelines.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    SkillsComponent,
    TimelinesComponent,
    AboutComponent,
    HeaderComponent,
    GeneralComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    NgbModule,
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: 'pl',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    SkillsService,
    TimelinesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
